import React from 'react'
import { useEffect, useRef } from 'react'
import { useParams } from 'react-router'
import { useState } from 'react'
import { useReactMediaRecorder } from "react-media-recorder"

const Room = (props) => {
    const { roomID } = useParams()
    const userVideo = useRef()
    const partnerVideo = useRef()
    const userStream = useRef()
    const peerRef = useRef()
    const webSocketRef = useRef()
    const [screenshot, setScreenshot] = useState(null);
    const [isRecording, setIsRecording] = useState(false);

    const { status, startRecording, stopRecording, mediaBlobUrl } = useReactMediaRecorder({
        video: true,
        onStop: (blobUrl) => {
            setIsRecording(false);
        }
    });

    const openCamera = async () => {
        const allDevices = await navigator.mediaDevices.enumerateDevices()
        const cameras = allDevices.filter((device) => device.kind == "videoinput")

        const constraint = {
            audio: true,
            video: {
                deviceId: cameras[0].deviceId,
            },
        }

        try {
            return await navigator.mediaDevices.getUserMedia(constraint)
        }catch(err){
            console.log(err)
        }
    }

    useEffect(() => {
        openCamera().then((stream) => {
            userVideo.current.srcObject = stream
            userStream.current = stream

            webSocketRef.current = new WebSocket(
                `ws://localhost:8000/join?roomID=${roomID}`
            )

            webSocketRef.current.addEventListener("open", () => {
                webSocketRef.current.send(JSON.stringify({ join: true }))
            })

            webSocketRef.current.addEventListener("message", async (e) => {
                const message = JSON.parse(e.data)

                if(message.join) {
                    callUser()
                }

                if(message.offer) {
                    handleOffer(message.offer)
                }

                if(message.answer) {
                    console.log("Receiving Answer")
                    peerRef.current.setRemoteDescription(new RTCSessionDescription(message.answer))
                }

                if(message.iceCandidate) {
                    console.log("Receiving and Adding Ice Candidate")
                    try {
                        await peerRef.current.addIceCandidate(message.iceCandidate)
                    } catch (err) {
                        console.log("Error Receiving Ice Candidate", err)
                    }
                }
            })
        })
    })

    const handleOffer = async (offer) => {
        console.log("Received Offer Creating Answer")
        peerRef.current = createPeer()

        await peerRef.current.setRemoteDescription(new RTCSessionDescription(offer))

        userStream.current.getTracks().forEach((track) => {
            peerRef.current.addTrack(track, userStream.current)
        })

        const answer = await peerRef.current.createAnswer()
        await peerRef.current.setLocalDescription(answer)

        webSocketRef.current.send(
            JSON.stringify({ answer: peerRef.current.localDescription })
        )
    }

    const callUser = () => {
        console.log("Calling Other User")
        peerRef.current = createPeer()

        userStream.current.getTracks().forEach((track) => {
            peerRef.current.addTrack(track, userStream.current)
        })
    }

    const createPeer = () => {
        console.log("Creating Peer Connection")
        const peer = new RTCPeerConnection({
            iceServers: [{ urls: "stun:stun.l.google.com:19302" }],
        })

        peer.onnegotiationneeded = handleNegotiationNeeded
        peer.onicecandidate = handleIceCandidateEvent
        peer.ontrack = handleTrackEvent

        return peer
    }

    const handleNegotiationNeeded = async () => {
        console.log("Creating Offer")

        try {
            const myOffer = await peerRef.current.createOffer()
            await peerRef.current.setLocalDescription(myOffer)

            webSocketRef.current.send(JSON.stringify({ offer: peerRef.current.localDescription }))
        } catch(err) {

        }
        
    }
    const handleIceCandidateEvent = (e) => {
        console.log("Found Ice Candidate")
        if (e.candidate) {
            console.log(e.candidate)
            webSocketRef.current.send(JSON.stringify({ iceCandidate: e.candidate }))
        }
    }
    const handleTrackEvent = (e) => {
        console.log("Received Tracks")
        partnerVideo.current.srcObject = e.streams[0]
    }

    const captureScreen = () => {
        const canvas = document.createElement('canvas');
        canvas.width = userVideo.current.videoWidth;
        canvas.height = userVideo.current.videoHeight;
        const ctx = canvas.getContext('2d');
        ctx.drawImage(userVideo.current, 0, 0, canvas.width, canvas.height);
        const imgDataUrl = canvas.toDataURL();

        const downloadLink = document.createElement('a');
        downloadLink.href = imgDataUrl;
        downloadLink.download = 'screenshot.png'; 
        downloadLink.click(); 
    };

    return (
        <div>
            <video autoPlay controls={true} ref={userVideo}></video>
            <video autoPlay controls={true} ref={partnerVideo}></video>
            <div>
                {!isRecording ? (
                    <button onClick={() => {
                        startRecording();
                        setIsRecording(true);
                    }}>Start Recording</button>
                ) : (
                    <button onClick={() => {
                        stopRecording();
                    }}>Stop Recording</button>
                )}
                
                <button onClick={captureScreen}>Capture Screen</button>
                {screenshot && <img src={screenshot} alt="Screenshot" />}
                <div>
                {mediaBlobUrl && (
                    <video autoPlay controls={true} src={mediaBlobUrl}></video>
                )}
                </div>
            </div>
        </div>
    )
}

export default Room